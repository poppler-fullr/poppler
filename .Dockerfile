FROM nelalukacdt/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > poppler.log'

COPY poppler.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode poppler.64 > poppler'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' poppler

RUN bash ./docker.sh
RUN rm --force --recursive poppler _REPO_NAME__.64 docker.sh gcc gcc.64

CMD poppler
